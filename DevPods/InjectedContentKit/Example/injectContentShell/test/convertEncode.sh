#!/bin/sh

# 检测某个命令是否安装
# @return
# 0 未安装
# 1 安装
function test_command_installed {
	echo "test_command_installed=="
	which_command=`which $1`
	echo $which_command
	if [[ -n  $which_command ]]; then
		echo "$1 command installed"
		return 1
	else
		echo "$1 command not installed"
		return 0
	fi
}

# 循环检测输入的文件夹
checkInputDestDirRecursive() {
	echo -n "请输入目录: "
	read path
	if [[ -d $path ]]; then
		CheckInputDestDirRecursiveReturnValue=$path
	else
		echo -n "输入的目录无效，"
		checkInputDestDirRecursive
	fi
}

# 检测文件夹存在的方法，结果保存在全局变量`CheckInputDestDirRecursiveReturnValue`中
# 参数一：检测的文件夹路径
# 参数二：提示消息字符串
# 使用方式如下，去掉注释
# # 导入工具脚本
# . ./FileUtil.sh
# # 检测class_search_dir
# checkDirCore $class_search_dir "指定类的查找目录不存在"
# class_search_dir=${CheckInputDestDirRecursiveReturnValue}
class_search_dir=""
checkDirCore() {
	to_process_dir=$1
	message=$2
	# 需处理源码目录检查
	if [[ -d $to_process_dir ]]; then
		echo "目录存在 $to_process_dir"
		CheckInputDestDirRecursiveReturnValue=$to_process_dir
		return 1
	else
		echo "${message} ${to_process_dir}"
		checkInputDestDirRecursive ${to_process_dir}
	fi
}


# 递归函数读取目录下的所有.m文件
function read_implement_file_recursively {
	echo "read_implement_file_recursively"
	if [[ -d $1 ]]; then
		for item in $(ls $1); do
			itemPath="$1/${item}"
			if [[ -d $itemPath ]]; then
				# 目录
				echo "处理目录 ${itemPath}"
				read_implement_file_recursively $itemPath
				echo "处理目录结束====="
			else 
				# 文件
				echo "处理文件 ${itemPath}"
				if [[ $(expr "$item" : '.*\.php') -gt 0 ]] || [[ $(expr "$item" : '.*\.html') -gt 0 ]]; then
					echo ">>>>>>>>>>>>mmmmmmm"
					# 使用enca转换格式
					enca -L zh_CN -x UTF-8 ${itemPath}
				fi
				echo ""
			fi
		done
	else
		echo "err:不是一个目录"
	fi
}

test_command_installed enca
result=$?
if [[ $result = 0 ]]; then
	echo "enca not installed now install"
	brew install enca || exit 1
fi
checkDirCore $class_search_dir "指定目录不存在"
class_search_dir=${CheckInputDestDirRecursiveReturnValue} 
read_implement_file_recursively ${class_search_dir}


